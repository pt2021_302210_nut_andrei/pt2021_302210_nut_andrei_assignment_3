package bll;

import presentation.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */


public class Controller implements ActionListener {
    private ClientMenu clientMenu;
    private OrderMenu orderMenu;
    private ProductMenu productMenu;
    private MainMenu mainMenu;

    /**
     * Prin intermediul constructorului se instantiaza cele 4 tipuri de meniuri existente si se adauga action listeners pentru butoanele de back din fiecare meniu
     */
    public Controller(){
        this.mainMenu = new MainMenu();
        this.orderMenu = new OrderMenu();
        this.productMenu = new ProductMenu();
        this.clientMenu = new ClientMenu();

        clientMenu.addBackActionListener(this::actionPerformed);
        orderMenu.addBackActionListener(this::actionPerformed);
        productMenu.addBackActionListener(this::actionPerformed);

        mainMenu.setVisible(true);
        mainMenu.addClientActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainMenu.setVisible(false);
                new ClientBLL(clientMenu);
            }
        });
        mainMenu.addOrderButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainMenu.setVisible(false);
                new OrderBLL(orderMenu);
            }
        });
        mainMenu.addProductActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainMenu.setVisible(false);
                new ProductBLL(productMenu);
            }
        });


    }


    /**
     * Metoda suprascrie actionPerformed pentru a se avea executia dorita a programului
     * @param e reprezinta evenimentul
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        this.clientMenu.setVisible(false);
        this.orderMenu.setVisible(false);
        this.productMenu.setVisible(false);
        this.mainMenu.setVisible(true);
    }
}
