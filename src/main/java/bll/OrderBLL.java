package bll;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Clients;
import model.Orders;
import model.Product;
import presentation.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class OrderBLL {

    private OrderMenu orderMenu;
    protected int index = 0;
    protected int billNumber = 1;

    /**
     * Prin intermediul constructorului se instantiaza interfata pentru Orders si se adauga action listeners fiecarui buton din interfata
     * @param orderMenu reprezinta instanta creata in Controller a obiectului de tip orderMenu
     */
    public OrderBLL(OrderMenu orderMenu){
        this.orderMenu = orderMenu;
        orderMenu.setVisible(true);

        orderMenu.addListActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderDAO x = new OrderDAO();
                ArrayList<String> colls = x.getColumns();
                ArrayList<Orders> myOrders = null;
                try {
                    myOrders = (ArrayList<Orders>) x.showAll();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                String[] columns = new String[colls.size()];
                int i = 0;
                for(String s : colls){
                    columns[i++] = s;
                }
                String[][] data = new String[100][100];
                int j = 0;
                i = 0;
                for(Orders s : myOrders){
                    data[i][j++] = String.valueOf(s.getId());
                    data[i][j++] = s.getOrdered_product();
                    data[i][j++] = String.valueOf(s.getQuantity());
                    i++;
                    j=0;
                }
                ListFrame listFrame = new ListFrame(data,columns);
                listFrame.setVisible(true);
            }
        });

        orderMenu.addAddOrderActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderAdder ords = new OrderAdder();
                ClientDAO x = new ClientDAO();
                ProductDAO y = new ProductDAO();
                OrderDAO z = new OrderDAO();
                List<Clients> allClients;
                List<Product> allProducts;
                try {
                    allClients = x.showAll();
                    allProducts = y.showAll();
                    DefaultComboBoxModel model1 = (DefaultComboBoxModel) ords.getClientComboBox().getModel();
                    DefaultComboBoxModel model2 = (DefaultComboBoxModel) ords.getProductComboBox().getModel();
                    model1.removeAllElements();
                    model2.removeAllElements();
                    for(Clients s : allClients){
                        model1.addElement(s.getId());
                    }
                    ords.getClientComboBox().setModel(model1);

                    for(Product s : allProducts){
                        model2.addElement(s.getId());
                    }
                    ords.getProductComboBox().setModel(model2);
                    ords.setVisible(true);
                    ords.addOrderButtonListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            int productId = (int) ords.getProductComboBox().getSelectedItem();
                            int clientId = (int)ords.getClientComboBox().getSelectedItem();
                            try {
                                Product rez = y.showById(productId).get(0);
                                if(ords.getQuantity() > rez.getQuantity()){
                                    JOptionPane.showMessageDialog(null,"Stock is too low for this order");
                                }
                                else{
                                    Orders newOrder = new Orders();
                                    newOrder.setId(++index);
                                    newOrder.setQuantity(ords.getQuantity());
                                    newOrder.setOrdered_product(rez.getName());
                                    z.insert(newOrder);
                                    y.delete(rez.getId());
                                    rez.setQuantity(rez.getQuantity() - ords.getQuantity());
                                    y.insert(rez);
                                    createBill(++billNumber,ords.getQuantity(),rez.getName());
                                    JOptionPane.showMessageDialog(null,"Success!");
                                }
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            }
                        }
                    });
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });

        orderMenu.addEditActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProcessView processView = new ProcessView();
                processView.setLblAddress("");
                processView.remove(processView.getTxtBox());
                processView.setLblName("Quantity");
                processView.setVisible(true);
                OrderDAO x = new OrderDAO();
                processView.addRunBtnListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Orders newOrder = new Orders();
                        newOrder.setId(processView.getID());
                        newOrder.setOrdered_product(processView.getThirdText());
                        newOrder.setQuantity(Integer.parseInt(processView.getSecondText()));
                        try {
                            x.update(newOrder);
                            JOptionPane.showMessageDialog(null,"Success!");
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }finally {
                            processView.setVisible(false);
                        }
                    }
                });

            }
        });

        orderMenu.addDeleteActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProcessView processView = new ProcessView();
                processView.setLblAddress("");
                processView.setLblName("");
                processView.remove(processView.getTxtBox());
                processView.remove(processView.getTxtBoxs());
                processView.setVisible(true);
                OrderDAO x = new OrderDAO();
                ProductDAO y = new ProductDAO();
                processView.addRunBtnListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            List<Orders> orderToDelete = x.showById(processView.getID());
                            List<Product> allProducts = y.showAll();

                            for(Product s : allProducts){
                                if(s.getName().equals(orderToDelete.get(0).getOrdered_product())){
                                    Product updatedProduct = new Product();
                                    updatedProduct.setQuantity(s.getQuantity() + orderToDelete.get(0).getQuantity());
                                    updatedProduct.setId(s.getId());
                                    updatedProduct.setName(s.getName());
                                    y.delete(s.getId());
                                    y.insert(updatedProduct);
                                }
                            }

                            x.delete(processView.getID());
                            JOptionPane.showMessageDialog(null,"Success!");
                            processView.setVisible(false);
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                });
            }
        });
    }
    //createBill(index,ords.getQuantity(),rez.getName());
    public void createBill(int index, int quantity, String name){
        StringBuilder message = new StringBuilder();
        message.append("Order no.: ");
        message.append(index);
        message.append("\nOrdered quantity: ");
        message.append(quantity);
        message.append("\nOrdered Product: ");
        message.append(name);
        StringBuilder billNo = new StringBuilder();
        billNo.append("bill_");
        billNo.append(index);
        billNo.append(".txt");
        try{
            File bill = new File(billNo.toString());
            if(bill.createNewFile()){
                FileWriter theWriter = new FileWriter(billNo.toString());
                theWriter.write(message.toString());
                theWriter.close();
            }
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
