package bll;

import dao.ClientDAO;
import dao.OrderDAO;
import model.Clients;
import model.Orders;
import presentation.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class ClientBLL {

    private ClientMenu clientMenu;

    /**
     * Prin intermediul constructorului se instantiaza un meniu de client si se atribuie action listeners la fiecare buton prezent in meniu
     * @param x
     */
    public ClientBLL(ClientMenu x){
        this.clientMenu = x;
        clientMenu.setVisible(true);
        clientMenu.addAddClientActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProcessView processView = new ProcessView();
                processView.setVisible(true);
                processView.addRunBtnListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ClientDAO x = new ClientDAO();
                        Clients newClient = new Clients();
                        newClient.setId(processView.getID());
                        newClient.setName(processView.getSecondText());
                        newClient.setAddress(processView.getThirdText());
                        try {
                            x.insert(newClient);
                            JOptionPane.showMessageDialog(null,"Success!");
                        } catch (SQLException throwables) {
                            JOptionPane.showMessageDialog(null,"Something went wrong. Check the data");
                            throwables.printStackTrace();
                        }
                        finally {
                            processView.setVisible(false);
                        }
                    }
                });
            }
        });

        clientMenu.addListActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientDAO x = new ClientDAO();
                ArrayList<String> colls = x.getColumns();
                ArrayList<Clients> myClients = null;
                try {
                    myClients = (ArrayList<Clients>) x.showAll();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                String[] columns = new String[colls.size()];
                int i = 0;
                for(String s : colls){
                    columns[i++] = s;
                }
                String[][] data = new String[100][100];
                int j = 0;
                i = 0;
                for(Clients s : myClients){
                    data[i][j++] = String.valueOf(s.getId());
                    data[i][j++] = s.getName();
                    data[i][j++] = String.valueOf(s.getAddress());
                    i++;
                    j=0;
                }
                ListFrame listFrame = new ListFrame(data,columns);
                listFrame.setVisible(true);
            }
        });

        clientMenu.addEditActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProcessView processView = new ProcessView();
                processView.setVisible(true);
                processView.addRunBtnListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ClientDAO x = new ClientDAO();
                        Clients newClient = new Clients();
                        newClient.setId(processView.getID());
                        newClient.setName(processView.getSecondText());
                        newClient.setAddress(processView.getThirdText());
                        try {
                            x.update(newClient);
                            JOptionPane.showMessageDialog(null,"Success!");
                        } catch (SQLException throwables) {
                            JOptionPane.showMessageDialog(null,"Something went wrong. Check the data");
                            throwables.printStackTrace();
                        }
                        finally {
                            processView.setVisible(false);
                        }
                    }
                });
            }
        });
        clientMenu.addDeleteActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProcessView processView = new ProcessView();
                processView.remove(processView.getTxtBoxs());
                processView.remove(processView.getTxtBox());
                processView.setLblAddress("");
                processView.setLblName("");
                processView.setVisible(true);
                processView.addRunBtnListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ClientDAO x = new ClientDAO();
                        try {
                            x.delete(processView.getID());
                            JOptionPane.showMessageDialog(null,"Success!");
                        } catch (SQLException throwables) {
                            JOptionPane.showMessageDialog(null,"Something went wrong. Check the data");
                            throwables.printStackTrace();
                        }
                        finally {
                            processView.setVisible(false);
                        }
                    }
                });
            }
        });

    }
}
