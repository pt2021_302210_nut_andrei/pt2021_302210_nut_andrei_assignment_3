package bll;

import dao.OrderDAO;
import dao.ProductDAO;
import model.Orders;
import model.Product;
import presentation.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class ProductBLL {

    private ProductMenu productMenu;

    /**
     * Prin intermediul constructorului se instantiaza interfata pentru Product si se adauga action listeners fiecarui buton din interfata
     * @param productMenu reprezinta instanta creata in Controller a obiectului de tip productMenu
     */
    public ProductBLL(ProductMenu productMenu){
        this.productMenu = productMenu;
        productMenu.setVisible(true);

        productMenu.addListActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProductDAO x = new ProductDAO();
                ArrayList<String> colls = x.getColumns();
                ArrayList<Product> myProducts = null;
                try {
                    myProducts = (ArrayList<Product>) x.showAll();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                String[] columns = new String[colls.size()];
                int i = 0;
                for(String s : colls){
                    columns[i++] = s;
                }
                String[][] data = new String[100][100];
                int j = 0;
                i = 0;
                for(Product s : myProducts){
                    data[i][j++] = String.valueOf(s.getId());
                    data[i][j++] = s.getName();
                    data[i][j++] = String.valueOf(s.getQuantity());
                    i++;
                    j=0;
                }
                ListFrame listFrame = new ListFrame(data,columns);
                listFrame.setVisible(true);
            }
        });

        productMenu.addAddProductActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProcessView processView = new ProcessView();
                processView.setLblAddress("Quantity");
                processView.setVisible(true);
                processView.addRunBtnListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ProductDAO x = new ProductDAO();
                        Product newClient = new Product();
                        newClient.setId(processView.getID());
                        newClient.setName(processView.getSecondText());
                        newClient.setQuantity(Integer.parseInt(processView.getThirdText()));
                        try {
                            x.insert(newClient);
                            JOptionPane.showMessageDialog(null,"Success!");
                        } catch (SQLException throwables) {
                            JOptionPane.showMessageDialog(null,"Something went wrong. Check the data");
                            throwables.printStackTrace();
                        }
                        finally {
                            processView.setVisible(false);
                        }
                    }
                });
            }
        });

        productMenu.addEditActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProcessView processView = new ProcessView();
                processView.setLblAddress("Quantity");
                processView.setVisible(true);
                processView.addRunBtnListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ProductDAO x = new ProductDAO();
                        Product newClient = new Product();
                        newClient.setId(processView.getID());
                        newClient.setName(processView.getSecondText());
                        newClient.setQuantity(Integer.parseInt(processView.getThirdText()));
                        try {
                            x.update(newClient);
                            JOptionPane.showMessageDialog(null,"Success!");
                        } catch (SQLException throwables) {
                            JOptionPane.showMessageDialog(null,"Something went wrong. Check the data");
                            throwables.printStackTrace();
                        }
                        finally {
                            processView.setVisible(false);
                        }
                    }
                });
            }
        });

        productMenu.addDeleteActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProcessView processView = new ProcessView();
                processView.setLblAddress("");
                processView.setLblName("");
                processView.remove(processView.getTxtBox());
                processView.remove(processView.getTxtBoxs());
                processView.setVisible(true);
                processView.addRunBtnListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ProductDAO x = new ProductDAO();
                        try {
                            x.delete(processView.getID());
                            JOptionPane.showMessageDialog(null,"Success!");
                        } catch (SQLException throwables) {
                            JOptionPane.showMessageDialog(null,"Something went wrong. Check the data");
                            throwables.printStackTrace();
                        }
                        finally {
                            processView.setVisible(false);
                        }
                    }
                });
            }
        });
    }
}
