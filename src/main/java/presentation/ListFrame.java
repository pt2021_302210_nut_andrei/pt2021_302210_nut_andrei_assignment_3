package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class ListFrame extends JFrame {
    private JLabel titleLabel;
    private JTable table;

    /**
     * Constructorul este folosit pentru a initializa fereastra unde urmeaza sa fie afisate datele din bazele de date
     * @param data se refera la datele ce apar in baza de date
     * @param columns se refera la coloanele care se regasesc in baza de date
     */
    public ListFrame(Object[][] data, Object[] columns) {
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 630, 290);

        getContentPane().setLayout(null);

        titleLabel = new JLabel("Order");
        titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
        titleLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
        titleLabel.setBounds(190, 11, 251, 42);
        getContentPane().add(titleLabel);

        table = new JTable(data,columns);
        table.setBounds(68, 64, 476, 155);
        getContentPane().add(table);
    }

    /**
     * Metoda getTable() returneaza referinta la tabelul cu date creat in interfata
     * @return referinta la tabelul creat in interfata
     */
    public JTable getTable(){
        return this.table;
    }

    /**
     * Metoda setTable() este o metoda prin intermediu careia se seteaza tabelul din aceasta clasa
     * @param x reprezinta un JTable
     */
    public void setTable(JTable x){
        this.table = x;
    }
}