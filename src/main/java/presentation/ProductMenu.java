package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class ProductMenu extends JFrame {
    private JButton btnList;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton addBtn;
    private JLabel titleLabel;
    private JButton backBtn;

    /**
     * Constructorul ProductMenu() creeaza fereastra principala de unde se poate alege ce actiune sa se realizeze asupra bazei de date Product
     */
    public ProductMenu(){

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 470, 435);

        getContentPane().setLayout(null);

        titleLabel = new JLabel("Product");
        titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
        titleLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
        titleLabel.setBounds(89, 11, 251, 42);
        getContentPane().add(titleLabel);

        addBtn = new JButton("add");
        addBtn.setBounds(32, 159, 89, 23);
        getContentPane().add(addBtn);

        btnEdit = new JButton("edit");
        btnEdit.setBounds(170, 159, 89, 23);
        getContentPane().add(btnEdit);

        btnDelete = new JButton("delete");
        btnDelete.setBounds(300, 159, 89, 23);
        getContentPane().add(btnDelete);

        btnList = new JButton("list");
        btnList.setBounds(170, 208, 89, 23);
        getContentPane().add(btnList);

        backBtn = new JButton("BACK");
        backBtn.setBounds(129, 313, 183, 42);
        getContentPane().add(backBtn);
    }

    /**
     * Metoda addAddProductActionListener este folosita pentru a adauga un action listener pentru butonul de edit din fereastra
     * @param actionListener
     */
    public void addAddProductActionListener(ActionListener actionListener){
        this.addBtn.addActionListener(actionListener);
    }

    /**
     * Metoda addEditActionListener este folosita pentru a adauga un action listener pentru butonul de edit din fereastra
     * @param actionListener
     */
    public void addEditActionListener(ActionListener actionListener){
        this.btnEdit.addActionListener(actionListener);
    }

    /**
     * Metoda addDeleteActionListener este folosita pentru a adauga un action listener pentru butonul de delete din fereastra
     * @param actionListener
     */
    public void addDeleteActionListener(ActionListener actionListener){
        this.btnDelete.addActionListener(actionListener);
    }

    /**
     * Metoda addListActionListener este folosita pentru a adauga un action listener pentru butonul de add din fereastra
     * @param actionListener
     */
    public void addListActionListener(ActionListener actionListener){
        this.btnList.addActionListener(actionListener);
    }

    /**
     * Metoda addBackActionListener este folosita pentru a adauga un action listener pentru butonul de back din fereastra
     * @param actionListener
     */
    public void addBackActionListener(ActionListener actionListener){
        this.backBtn.addActionListener(actionListener);
    }
}
