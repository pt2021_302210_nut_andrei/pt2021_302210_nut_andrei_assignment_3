package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class OrderMenu extends JFrame {
    private JButton btnList;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton addBtn;
    private JLabel titleLabel;
    private JButton backBtn;

    /**
     * Constructorul OrderMenu() este folosit pentru a initializa fereastra principala pentru baza de date Orders
     */
    public OrderMenu(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 470, 435);

        getContentPane().setLayout(null);

        titleLabel = new JLabel("Order");
        titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
        titleLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
        titleLabel.setBounds(89, 11, 251, 42);
        getContentPane().add(titleLabel);

        addBtn = new JButton("add");
        addBtn.setBounds(32, 159, 89, 23);
        getContentPane().add(addBtn);

        btnEdit = new JButton("edit");
        btnEdit.setBounds(170, 159, 89, 23);
        getContentPane().add(btnEdit);

        btnDelete = new JButton("delete");
        btnDelete.setBounds(300, 159, 89, 23);
        getContentPane().add(btnDelete);

        btnList = new JButton("list");
        btnList.setBounds(170, 208, 89, 23);
        getContentPane().add(btnList);

        backBtn = new JButton("BACK");
        backBtn.setBounds(129, 313, 183, 42);
        getContentPane().add(backBtn);
    }

    /**
     * Metoda addAddOrderActionListener este folosita pentru a adauga un action listener butonului de add
     * @param actionListener reprezinta action listener-ul care se doreste sa fie asignat butonului
     */
    public void addAddOrderActionListener(ActionListener actionListener){
        this.addBtn.addActionListener(actionListener);
    }
    /**
     * Metoda addEditActionListener este folosita pentru a adauga un action listener butonului de edit
     * @param actionListener reprezinta action listener-ul care se doreste sa fie asignat butonului
     */
    public void addEditActionListener(ActionListener actionListener){
        this.btnEdit.addActionListener(actionListener);
    }
    /**
     * Metoda addDeleteActionListener este folosita pentru a adauga un action listener butonului de delete
     * @param actionListener reprezinta action listener-ul care se doreste sa fie asignat butonului
     */
    public void addDeleteActionListener(ActionListener actionListener){
        this.btnDelete.addActionListener(actionListener);
    }
    /**
     * Metoda addListActionListener este folosita pentru a adauga un action listener butonului de list
     * @param actionListener reprezinta action listener-ul care se doreste sa fie asignat butonului
     */
    public void addListActionListener(ActionListener actionListener){
        this.btnList.addActionListener(actionListener);
    }
    /**
     * Metoda addBackActionListener este folosita pentru a adauga un action listener butonului de back
     * @param actionListener reprezinta action listener-ul care se doreste sa fie asignat butonului
     */
    public void addBackActionListener(ActionListener actionListener){
        this.backBtn.addActionListener(actionListener);
    }
}
