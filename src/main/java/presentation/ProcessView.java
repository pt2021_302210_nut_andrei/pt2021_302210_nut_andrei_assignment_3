package presentation;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class ProcessView extends JFrame {
    private JTextField secondField;
    private JTextField thirdField;
    private JTextField idField;
    private JLabel idLbl;
    private JLabel lblName;
    private JLabel lblAddress;
    private JButton runBtn;

    /**
     * Constructorul ProcessView() este folosit pentru a initializa fereastra prin intermediul careia se efectueaza editari la nivel de baza de date
     */
    public ProcessView(){
        setBounds(100, 100, 600, 285);
        getContentPane().setLayout(null);

        idField = new JTextField();
        idField.setBounds(36, 120, 86, 20);
        getContentPane().add(idField);
        idField.setColumns(10);

        secondField = new JTextField();
        secondField.setColumns(10);
        secondField.setBounds(237, 120, 86, 20);
        getContentPane().add(secondField);

        thirdField = new JTextField();
        thirdField.setColumns(10);
        thirdField.setBounds(441, 120, 86, 20);
        getContentPane().add(thirdField);

        idLbl = new JLabel("ID");
        idLbl.setHorizontalAlignment(SwingConstants.CENTER);
        idLbl.setBounds(36, 95, 86, 14);
        getContentPane().add(idLbl);

        lblName = new JLabel("NAME");
        lblName.setHorizontalAlignment(SwingConstants.CENTER);
        lblName.setBounds(237, 95, 86, 14);
        getContentPane().add(lblName);

        lblAddress = new JLabel("ADDRESS");
        lblAddress.setHorizontalAlignment(SwingConstants.CENTER);
        lblAddress.setBounds(441, 95, 86, 14);
        getContentPane().add(lblAddress);

        runBtn = new JButton("run");
        runBtn.setBounds(237, 212, 89, 23);
        getContentPane().add(runBtn);
    }

    /**
     * Metoda addRunBtnListener este folosita pentru a adauga un action listener butonului de run
     * @param actionListener reprezinta action listener-ul care se doreste sa fie asignat butonului
     */
    public void addRunBtnListener(ActionListener actionListener){
        this.runBtn.addActionListener(actionListener);
    }

    /**
     * Metoda getID este folosita pentru a scoate id-ul din prima casuta text din fereastra
     * @return id-ul aflat in prima casuta text
     */
    public int getID(){
        return Integer.parseInt(this.idField.getText());
    }

    /**
     * Metoda getSecondText este folosita pentru a extrage textul din a doua casuta text din fereastra
     * @return textul din a doua casuta text
     */
    public String getSecondText(){
        return this.secondField.getText();
    }

    /**
     * Metoda getThirdText este folosita pentru a extrage textul din a treia casuta text din fereastra
     * @return textul din a treia casuta text
     */
    public String getThirdText(){
        return this.thirdField.getText();
    }

    /**
     * Metoda setLblName este folosita pentru schimbarea textului afisat in cadrul label-ului de nume
     * @param s reprezinta String-ul trimis ca parametru ce se doreste sa apara in label
     */
    public void setLblName(String s){
        this.lblName.setText(s);
    }

    /**
     * Metoda getTxtBox este folosita pentru a returna referinta la cel de-al doilea text box
     * @return referinta la cel de-al doilea text box
     */
    public JTextField getTxtBox(){
        return this.thirdField;
    }

    /**
     * Metoda getTxtBoxs este folosita pentru a returna referinta la cel de-al treilea text box
     * @return referinta la cel de-al treilea text box
     */
    public JTextField getTxtBoxs(){
        return this.secondField;
    }

    /**
     * Metoda setLblAddress este folosita pentru a schimba textul din label-ul cu textul initial "Address"
     * @param s reprezinta String-ul cu care se doreste sa fie inlocuit textul initial din label
     */
    public void setLblAddress(String s){
        this.lblAddress.setText(s);
    }
}
