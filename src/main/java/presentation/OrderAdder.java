package presentation;

import model.Orders;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class OrderAdder extends JFrame {
    private JLabel titleLabel;
    private JComboBox clientBox;
    private JComboBox productBox;
    private JButton orderBtn;
    private JTextField quantityField;

    /**
     * Constructorul OrderAdder() este folosit pentru a initializa fereastra de adaugare de comenzi
     */
    public OrderAdder() {
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 630, 290);

        getContentPane().setLayout(null);

        titleLabel = new JLabel("Order");
        titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
        titleLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
        titleLabel.setBounds(190, 11, 251, 42);
        getContentPane().add(titleLabel);

        clientBox = new JComboBox();
        clientBox.setBounds(148, 93, 56, 42);
        getContentPane().add(clientBox);

        productBox = new JComboBox();
        productBox.setBounds(148, 146, 56, 42);
        getContentPane().add(productBox);

        JLabel lblNewLabel = new JLabel("Select Client ID");
        lblNewLabel.setBounds(33, 93, 105, 42);
        getContentPane().add(lblNewLabel);

        JLabel lblSelectProcutId = new JLabel("Select Procut ID");
        lblSelectProcutId.setBounds(33, 146, 105, 42);
        getContentPane().add(lblSelectProcutId);

        orderBtn = new JButton("Order");
        orderBtn.setBounds(427, 129, 89, 23);
        getContentPane().add(orderBtn);

        JLabel lblNewLabel_1 = new JLabel("Quantity");
        lblNewLabel_1.setBounds(33, 199, 80, 23);
        getContentPane().add(lblNewLabel_1);

        quantityField = new JTextField();
        quantityField.setBounds(142, 202, 86, 20);
        getContentPane().add(quantityField);
        quantityField.setColumns(10);
    }

    /**
     * Metoda addOrderButtonListener este folosita pentru a adauga un action listener butonului de order
     * @param actionListener reprezinta action listener-ul care se doreste sa fie asignat butonului
     */
    public void addOrderButtonListener(ActionListener actionListener){
        this.orderBtn.addActionListener(actionListener);
    }

    /**
     * Metoda getClientComboBox() este folosita pentru a returna referinta la combo box-ul corespunzator id-ului clientului
     * @return referinta la combo box
     */
    public JComboBox<Integer> getClientComboBox(){
        return this.clientBox;
    }

    /**
     * Metoda getProductComboBox() este folosita pentru a returna referinta la combo box-ul corespunzator id-ului produsului
     * @return referinta la combo box
     */
    public JComboBox<Integer> getProductComboBox(){
        return this.productBox;
    }

    /**
     * Metoda getQuantity este folosita pentru a extrage cantitatea dorita in comanda
     * @return un intreg ce reprezinta cantitatea introdusa din text field
     */
    public int getQuantity(){
        return Integer.parseInt(quantityField.getText());
    }

}
