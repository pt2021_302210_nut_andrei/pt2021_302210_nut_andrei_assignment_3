package presentation;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class MainMenu extends JFrame {

    private JButton btnOrders;
    private JButton btnProducts;
    private JButton clientBtn;

    /**
     * Constructorul MainMenu() este folosit pentru a initializa fereastra de unde se alege pe ce tabela se doreste sa se lucreze mai departe
     */
    public MainMenu(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 470, 435);

        getContentPane().setLayout(null);

        clientBtn = new JButton("client");
        clientBtn.setBounds(21, 92, 106, 60);
        getContentPane().add(clientBtn);

        btnProducts = new JButton("products");
        btnProducts.setBounds(170, 92, 106, 60);
        getContentPane().add(btnProducts);

        btnOrders = new JButton("orders");
        btnOrders.setBounds(318, 92, 106, 60);
        getContentPane().add(btnOrders);
    }

    /**
     * Metoda addClientActionListener este folosita pentru a adauga un action listener butonului de client
     * @param actionListener reprezinta action listener-ul care se doreste sa fie asignat butonului
     */
    public void addClientActionListener(ActionListener actionListener){
        this.clientBtn.addActionListener(actionListener);
    }
    /**
     * Metoda addProductActionListener este folosita pentru a adauga un action listener butonului de products
     * @param actionListener reprezinta action listener-ul care se doreste sa fie asignat butonului
     */
    public void addProductActionListener(ActionListener actionListener){
        this.btnProducts.addActionListener(actionListener);
    }
    /**
     * Metoda addOrderButtonListener este folosita pentru a adauga un action listener butonului de order
     * @param actionListener reprezinta action listener-ul care se doreste sa fie asignat butonului
     */
    public void addOrderButtonListener(ActionListener actionListener){
        this.btnOrders.addActionListener(actionListener);
    }
}
