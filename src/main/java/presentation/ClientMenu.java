package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class ClientMenu extends JFrame {
    private JButton btnList;
    private JButton btnDelete;
    private JButton btnEdit;
    private JButton addBtn;
    private JLabel titleLabel;
    private JButton backBtn;

    /**
     * Constructorul ClientMenu() este folosit pentru a instantia fereastra pentru meniul bazei de date Clients
     */
    public ClientMenu(){

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 470, 435);
        //getContentPane().setBorder(new EmptyBorder(5,5,5,5));

        getContentPane().setLayout(null);

        titleLabel = new JLabel("Client");
        titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
        titleLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
        titleLabel.setBounds(89, 11, 251, 42);
        getContentPane().add(titleLabel);

        addBtn = new JButton("add");
        addBtn.setBounds(32, 159, 89, 23);
        getContentPane().add(addBtn);

        btnEdit = new JButton("edit");
        btnEdit.setBounds(170, 159, 89, 23);
        getContentPane().add(btnEdit);

        btnDelete = new JButton("delete");
        btnDelete.setBounds(300, 159, 89, 23);
        getContentPane().add(btnDelete);

        btnList = new JButton("list");
        btnList.setBounds(170, 208, 89, 23);
        getContentPane().add(btnList);

        backBtn = new JButton("BACK");
        backBtn.setBounds(129, 313, 183, 42);
        getContentPane().add(backBtn);
    }

    public void addAddClientActionListener(ActionListener actionListener){
        this.addBtn.addActionListener(actionListener);
    }
    public void addEditActionListener(ActionListener actionListener){
        this.btnEdit.addActionListener(actionListener);
    }
    public void addDeleteActionListener(ActionListener actionListener){
        this.btnDelete.addActionListener(actionListener);
    }
    public void addListActionListener(ActionListener actionListener){
        this.btnList.addActionListener(actionListener);
    }
    public void addBackActionListener(ActionListener actionListener){
        this.backBtn.addActionListener(actionListener);
    }
}
