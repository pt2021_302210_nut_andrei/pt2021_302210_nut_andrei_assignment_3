package connection;

import java.sql.*;
import java.util.logging.Logger;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class ConnectionFactory {
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/tp";
    private static final String USER = "root";
    private static final String PASS = "admin";

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    private ConnectionFactory(){
        try{
            Class.forName(DRIVER);
            Connection myCon = createConnection();
        }catch(ClassNotFoundException | SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Metoda createConnection creaza o conexiune la baza de date
     * @return returneaza conexiunea la SGBD
     * @throws SQLException reprezinta exceptia aruncata in caz de eroare
     */

    private Connection createConnection() throws SQLException {
        Connection conn = DriverManager.getConnection(DBURL,USER,PASS);
        return conn;
    }

    /**
     * Metoda getConnection este un getter pentru conexiunea creata
     * @return returneaza conexiunea la SGBD
     * @throws SQLException reprezinta exceptia aruncata in caz de eroare
     */
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DBURL,USER,PASS);
    }

    /**
     * Metoda close este folosita pentru a inchide conexiunea
     * @param connection reprezinta conexiunea ce se doreste sa fie inchisa
     * @throws SQLException reprezinta exceptia aruncata in caz de eroare
     */
    public static void close(Connection connection) throws SQLException {
        connection.close();
    }

    /**
     * Metoda close este folosita pentru a inchide un statement
     * @param statement reprezinta statement-ul ce se doreste sa fie inchis
     * @throws SQLException reprezinta exceptia aruncata in caz de eroare
     */
    public static void close(Statement statement) throws SQLException {
        statement.close();
    }

    /**
     * Metoda close este folosita pentru a inchide un resultSet
     * @param resultSet reprezinta resultSet-ul ce se doreste sa fie inchis
     * @throws SQLException reprezinta exceptia aruncata in caz de eroare
     */
    public static void close(ResultSet resultSet) throws SQLException {
        resultSet.close();
    }
}
