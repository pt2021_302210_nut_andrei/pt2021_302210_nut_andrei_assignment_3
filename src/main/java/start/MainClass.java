package start;

import bll.Controller;

import java.sql.SQLException;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class MainClass {

    public static void main(String[] args) throws SQLException {
        new Controller();
    }
}
