package model;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class Product {
    private int id;
    private String name;
    private int quantity;

    /**
     * Metoda setId este un setter pentru id-ul unui obiect de tip Product
     * @param id reprezinta id-ul cu care se doreste sa se schimbe id-ul curent
     */
    public void setId(int id){
        this.id = id;
    }

    /**
     * Metoda setName este un setter pentru numele unui obiect de tip Product
     * @param name reprezinta numele cu care se doreste sa se schimbe numele curent
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Metoda setQuantity este un setter pentru quantity-ul unui obiect de tip Product
     * @param quantity reprezinta quantity-ul cu care se doreste sa se schimbe quantity-ul curent
     */
    public void setQuantity(int quantity){
        this.quantity = quantity;
    }

    /**
     * Metoda GetId este un getter pentru id-ul obiectului de tip Product
     * @return returneaza id-ul obiectului curent
     */
    public int getId(){
        return this.id;
    }

    /**
     * Metoda getName este un getter pentru numele obiectului de tip Product
     * @return returneaza numele produsului
     */
    public String getName(){
        return this.name;
    }

    /**
     * Metoda getQuantity este un getter pentru quantity
     * @return returneaza quantity-ul produsului
     */
    public int getQuantity(){
        return this.quantity;
    }
}
