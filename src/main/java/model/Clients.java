package model;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class Clients {
    private int id;
    private String name;
    private String address;

    /**
     * Metoda setId este un setter pentru id
     * @param id reprezinta id-ul nou
     */
    public void setId(int id){
        this.id = id;
    }

    /**
     * Metoda setName reprezinta un setter pentru nume
     * @param name reprezinta numele nou
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Metoda setAddress reprezinta un setter pentru adresa
     * @param address reprezinta adresa noua
     */
    public void setAddress(String address){
        this.address = address;
    }

    /**
     * Metoda getId reprezinta un getter pentru id
     * @return returneaza un intreg ce reprezinta id-ul
     */
    public int getId(){
        return this.id;
    }

    /**
     * Metoda getName reprezinta un getter pentru nume
     * @return returneaza un String ce reprezinta numele
     */
    public String getName(){
        return this.name;
    }

    /**
     * Metoda getAddress este un getter pentru adresa
     * @return returneaza un String ce reprezinta adresa
     */
    public String getAddress(){
        return this.address;
    }
}
