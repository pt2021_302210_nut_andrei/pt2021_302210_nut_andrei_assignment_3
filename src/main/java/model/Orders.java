package model;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class Orders {
    private int id;
    private int quantity;
    private String ordered_product;

    /**
     * Metoda setId este un setter pentru id-ul obiectului
     * @param order_id reprezinta id-ul cu care se doreste sa fie schimbat id-ul curent
     */
    public void setId(int order_id){
        this.id = order_id;
    }

    /**
     * Metoda setQuantity este un setter pentru quantity-ul obiectului
     * @param quantity reprezinta quantity-ul cu care se doreste sa fie schimbat quantity-ul curent
     */
    public void setQuantity(int quantity){
        this.quantity = quantity;
    }

    /**
     * Metoda setOrdered_product este un setter pentru numele produsului comandat
     * @param ordered_product reprezinta numele cu care se doreste sa se schimbe numele curent
     */
    public void setOrdered_product(String ordered_product){
        this.ordered_product = ordered_product;
    }

    /**
     * Metoda getQuantity este un getter pentru quantity-ul din comanda
     * @return returneaza un intreg ce reprezinta cantitatea de produs comandata
     */
    public int getQuantity(){
        return this.quantity;
    }

    /**
     * Metoda getId este un getter pentru id-ul comenzii
     * @return returneaza o valoare ce reprezinta id-ul comenzii
     */
    public int getId(){
        return this.id;
    }

    /**
     * Metoda getOrdered_product este un getter pentru numele obiectului comandat
     * @return returneaza un string ce reprezinta numele produsului comandat
     */
    public String getOrdered_product(){
        return this.ordered_product;
    }
}
