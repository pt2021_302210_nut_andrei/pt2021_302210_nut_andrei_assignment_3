package dao;

import connection.ConnectionFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Nut Andrei Sebastian
 * @since 25/04/2021
 */

public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    private final Class<T> object;

    @SuppressWarnings("unchecked")
    public AbstractDAO(){
        this.object = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Metoda createViewQuery e folosita pentru a crea un query cu ajutorul caruia se returneaza toate datele introduse in SGBD
     * @return un String ce contine query-ul
     */
    private String createViewQuery(){
        StringBuilder rez = new StringBuilder();
        rez.append("SELECT * FROM ");
        rez.append(object.getSimpleName());
        return rez.toString();
    }

    /**
     * Metoda createSelectById e folosita pentru a returna valorile din SGBD care au id-ul egal cu ovaloare trimisa prin parametru
     * @param id reprezinta valoarea id-ului cautat in SGBD
     * @return un String ce contine query-ul
     */
    private String createSelectById(int id){
        StringBuilder rez = new StringBuilder();
        rez.append("SELECT * FROM ");
        rez.append(object.getSimpleName());
        rez.append(" WHERE ID = ");
        rez.append(id);
        return rez.toString();
    }

    /**
     * Metoda createInsertQuery e folosita pentru a crea un query folosit la inserarea valorilor in SGBD
     * @return un String ce contine query-ul
     */
    private String createInsertQuery(){
        StringBuilder rez = new StringBuilder();
        rez.append("INSERT INTO ");
        rez.append(object.getSimpleName());
        rez.append(" VALUES (?,?,?)");
        return rez.toString();
    }

    /**
     * Metoda createDeleteQuery e folosita pentru a crea un query folosit la stergerea unor date din SGBD
     * @return un String ce contine query-ul
     */
    private String createDeleteQuery(){
        StringBuilder rez = new StringBuilder();
        rez.append("DELETE FROM ");
        rez.append(object.getSimpleName());
        rez.append(" WHERE ID = ?");
        return rez.toString();
    }

    /**
     * Metoda createUpdateQuery e folosita pentru a crea un query folosit la update-ul unor valori sin SGBD
     * @param str1 reprezinta prima coloana unde se va schinba valoarea
     * @param str2 reprezinta a doua coloana unde se va schimba valoarea
     * @return un String ce contine query-ul
     */
    private String createUpdateQuery(String str1, String str2){
        StringBuilder rez = new StringBuilder();
        rez.append("UPDATE ");
        rez.append(object.getSimpleName());
        rez.append(" SET ");
        rez.append(str1);
        rez.append(" = ?, ");
        rez.append(str2);
        rez.append(" = ? WHERE ID = ?");
        return rez.toString();
    }

    /**
     * Metoda update este folosita pentru a updata valori din SGBD
     * @param entry reprezinta valoarea ce doreste sa fie updatata din sgbd
     * @throws SQLException exceptiea aruncata in caz de eroare
     */
    public void update(T entry) throws SQLException{
        Connection conn = null;
        PreparedStatement stat = null;
        String str1 = "",str2 = "";
        for(Field field: object.getDeclaredFields()){
            if(!field.getName().equals("id")){
                if(str1.equals("")){
                    str1 = field.getName();
                }
                else{
                    str2 = field.getName();
                }
            }
        }
        String query = createUpdateQuery(str1,str2);
        try{
            conn = ConnectionFactory.getConnection();
            stat = conn.prepareStatement(query);
            int i = 1;
            for(Field field : object.getDeclaredFields()){
                if(!field.getName().equals("id")){
                    PropertyDescriptor prop = new PropertyDescriptor(field.getName(),object);
                    Method met = prop.getReadMethod();
                    stat.setObject(i++,met.invoke(entry));
                }
                else{
                    PropertyDescriptor prop = new PropertyDescriptor(field.getName(),object);
                    Method met = prop.getReadMethod();
                    stat.setObject(3,met.invoke(entry));
                }
            }
            stat.executeUpdate();
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        } finally{
            ConnectionFactory.close(conn);
            ConnectionFactory.close(stat);
        }
    }

    /**
     * Metoda folosita pentru stergerea unei linii din sgbd
     * @param id reprezinta criteriul de stergere al liniei
     * @throws SQLException exceptia aruncata in caz de eroare
     */
    public void delete(int id) throws SQLException {
        Connection conn = null;
        PreparedStatement stat = null;
        String query = createDeleteQuery();
        try{
            conn = ConnectionFactory.getConnection();
            stat = conn.prepareStatement(query);
            stat.setInt(1,id);
            stat.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally{
            ConnectionFactory.close(conn);
            ConnectionFactory.close(stat);
        }
    }

    /**
     * Metoda folosita pentru inserarea unui entry nou in SGBD
     * @param entry reprezinta valorile ce doresc sa fie introduse in SGBD
     * @throws SQLException exceptia aruncata in caz de eroare
     */
    public void insert(T entry) throws SQLException {
        Connection conn = null;
        PreparedStatement stat = null;
        String query = createInsertQuery();
        try{
            conn = ConnectionFactory.getConnection();
            stat = conn.prepareStatement(query);
            int param = 1;
            for(Field field : object.getDeclaredFields()){
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(),object);
                Method method = propertyDescriptor.getReadMethod();
                stat.setObject(param++,method.invoke(entry));
            }
            stat.executeUpdate();

        } catch (SQLException | IntrospectionException | InvocationTargetException | IllegalAccessException throwables) {
            LOGGER.log(Level.WARNING, object.getName()+"DAO:Insert " + throwables.getMessage());
        }finally{
            ConnectionFactory.close(conn);
            ConnectionFactory.close(stat);
        }
    }

    /**
     * Metoda este folosita pentru a converti un resultSet la o lista
     * @param resultSet reprezinta set-ul ce doreste sa fie convertit
     * @return returneaza o lista cu valorile din set
     */
    private List<T> createObjects(ResultSet resultSet){
        List<T> list = new ArrayList<T>();

        try{
            while(resultSet.next()){
                T instance = object.newInstance();
                for(Field field : object.getDeclaredFields()){
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor prop = new PropertyDescriptor(field.getName(),object);
                    Method method = prop.getWriteMethod();
                    method.invoke(instance,value);
                }
                list.add(instance);
            }
        } catch (SQLException | InstantiationException | IllegalAccessException | IntrospectionException | InvocationTargetException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    /**
     * Metoda este folosita pentru a extrage toate datele din SGBD
     * @return o lista cu datele din SGBD
     * @throws SQLException exceptie aruncata in caz de eroare
     */
    public List<T> showAll() throws SQLException {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet resultSet = null;
        String query = createViewQuery();
        try{
            conn = ConnectionFactory.getConnection();
            stat = conn.prepareStatement(query);
            resultSet = stat.executeQuery();
            return createObjects(resultSet);

        } catch (SQLException throwables) {
            LOGGER.log(Level.WARNING,object.getName() + "DAO:showAll " + throwables.getMessage());
        }finally{
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(stat);
            ConnectionFactory.close(conn);
        }
        return null;
    }

    /**
     * Metoda este folosita pentru a extrage datele din SGBD dupa id
     * @param id reprezinta id-ul dupa care se face extragerea
     * @return o lista cu datele extrase
     * @throws SQLException exceptie aruncata in caz de eroare
     */
    public List<T> showById(int id) throws SQLException{
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet resultSet = null;
        String query = createSelectById(id);
        try{
            conn = ConnectionFactory.getConnection();
            stat = conn.prepareStatement(query);
            resultSet = stat.executeQuery();
            return createObjects(resultSet);
        }catch(SQLException x){
            LOGGER.log(Level.WARNING,object.getName() + "DAO:showById " + x.getMessage());
        }finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(stat);
            ConnectionFactory.close(conn);
        }
        return null;
    }

    /**
     * Metoda folosita pentru a extrage numele coloanelor din SGBD
     * @return un ArrayList ce contine numele coloanelor din SGBD
     */
    public ArrayList<String> getColumns(){
        ArrayList<String> rez = new ArrayList<String>();
        for(Field field : object.getDeclaredFields()){
            rez.add(field.toString());
        }
        return rez;
    }
}
